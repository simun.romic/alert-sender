# alert-sender

APIs for sending custom events to SignalFx

## Requirements

- setup SignalFx account
- change SIGNAFX_TOKEN and SIGNAL_FX_REALM in config.env file with data from SignalFx once registered

## Running locally

- run `npm i` -> this will install project dependencies
- run `npm run build` -> this will build project and recreate build folder with compiled Javascript
- run `npm run start` -> this will start app locally

## Docker script
- run with docker.sh script with parameters:
    - build ( ./docker.sh build ) -> to build project and docker image
    - run ( ./docker.sh run ) -> to build project, docker image and run container
    - shell ( ./docker.sh shell) -> to build project, docker image, run container and start sh in terminal