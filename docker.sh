#!/bin/bash

NAME=$(basename "alert-sender")
PORT=3000

main() {
    case $1 in
        "build")
            docker_build
            ;;
        "run")
            docker_run
            ;;
        "shell")
            docker_shell
            ;;
        *)
            usage
            exit 1
            ;;
    esac
}

usage() {
    echo "Usage:"
    echo "$0 build | run | shell"
}

docker_build () {
    docker build -t $NAME .
}

docker_run () {
    docker_build
    echo "Forwarding to port $PORT"
    docker run -d --rm -ti --name $NAME -p $PORT:$PORT $NAME
}

docker_shell () {
    docker_run
    docker exec -i -t $NAME /bin/sh
}

main $1
