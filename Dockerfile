# stage 1 building
FROM node:10-alpine as builder
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# stage 2 running app
FROM node:10-alpine
WORKDIR /app
COPY package*.json ./

RUN npm install --production

COPY --from=builder /app/build ./build

EXPOSE 3000

CMD npm run start