/**
 * Start the Express Web-Server
 *
 */

// setup dotenv to read environmental varialbles form .env files
import * as dotenv from 'dotenv';
dotenv.config({ path:__dirname+'/config.env' });

import AppServer from './AppServer';

// port where express app will be started at
const PORT: number = 3000;

const exampleServer: AppServer = new AppServer();
exampleServer.start(PORT);
