import { SignalFxEvent } from '../models/SignalFxEvent';
import { Event } from '../models';
import { classToPlain } from 'class-transformer';

/**
 * Transform `Event` type to `SignalFxEvent`
 * @param event
 */
export function toSignalFxEvent(event: Event): SignalFxEvent {
    return new SignalFxEvent(classToPlain(event));
};