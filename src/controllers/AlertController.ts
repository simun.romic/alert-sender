/**
 * Alert controller
 *
 */

import { Request, Response } from 'express';
import { Controller, Post, Middleware } from '@overnightjs/core';
import { Logger } from '@overnightjs/logger';
import { Event } from '../models';
import { validationMiddleware } from '../middlewares';
import { plainToClass } from 'class-transformer';
import { NextFunction } from 'connect';
import { HttpException } from '../exceptions';
import { SignalFxService } from '../services';

@Controller('api')
export class AlertController {

    private readonly logger: Logger;

    private readonly signalFxService: SignalFxService;

    constructor() {
        this.logger = new Logger();
        this.signalFxService = new SignalFxService();
    }

    @Post('event')
    @Middleware(validationMiddleware(Event, false))
    private async event(req: Request, res: Response, next: NextFunction): Promise<void> {
        this.logger.info(JSON.stringify(req.body));
        const event: Event = plainToClass(Event, req.body as Event);
        this.logger.info(`event: ${JSON.stringify(event)}`);

        try {
            await this.signalFxService.sendEvent(event);
            res.status(204).send();
        } catch (err) {
            next(new HttpException(500, err.message));
        }        
    }
}