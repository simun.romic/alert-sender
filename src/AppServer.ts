/**
 * ExpressJS server
 *
 */
import * as bodyParser from 'body-parser';
import * as controllers from './controllers';
import errorMiddleware from './middlewares/error.middleware';

import { Server } from '@overnightjs/core';
import { Logger } from '@overnightjs/logger';

class AppServer extends Server {
    private readonly logger: Logger;
    private readonly SERVER_STARTED = 'Server started on port: ';

    constructor() {
        super();

        this.logger = new Logger();

        this.initializeMiddlewares();
        this.initializeControllers();
        this.initializeErrorHandling();
    }

    private initializeControllers(): void {

        const ctlrInstances = [];

        for (const name in controllers) {
            if (controllers.hasOwnProperty(name)) {
                const controller = (controllers as any)[name];
                ctlrInstances.push(new controller());
            }
        }

        super.addControllers(ctlrInstances);
    }

    private initializeMiddlewares() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: true}));
    }

    private initializeErrorHandling() {
        this.app.use(errorMiddleware);
    }

    public start(port: number): void {

        this.app.listen(port, () => {
            this.logger.imp(this.SERVER_STARTED + port);
        });
    }
}

export default AppServer;