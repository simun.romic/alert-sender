import { NextFunction, Request, Response } from 'express';
import { HttpException } from '../exceptions';

/**
 * Error handler middleware
 * @param error
 * @param request
 * @param response
 * @param next
 */
function errorMiddleware(error: HttpException, request: Request, response: Response, next: NextFunction) {
  const status = error.status || 500;
  const message = error.message || 'Internal server error';
  response
    .status(status)
    .send({
      message,
      status,
    });
}

export default errorMiddleware;