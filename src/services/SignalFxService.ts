import { Logger } from '@overnightjs/logger';
import axios, { AxiosResponse } from "axios";
import { toSignalFxEvent } from '../transformers';
import { Event } from '../models';

export class SignalFxService {
    private readonly logger: Logger;

    private readonly config: any;

    private readonly SIGNALFX_EVENT_API: string;

    constructor() {
        this.logger = new Logger();
        this.config = {
            headers: { 
                'Content-Type': 'application/json',
                'X-SF-TOKEN': process.env.SIGNAFX_TOKEN
            },
            responseType: 'json'
        };
        const SIGNAL_FX_REALM: string = process.env.SIGNAL_FX_REALM || '';
        this.SIGNALFX_EVENT_API = `https://ingest.${SIGNAL_FX_REALM}.signalfx.com/v2/event`;
    }
    
    public async sendEvent(event: Event): Promise<AxiosResponse> {   
        const signalfxEvent = toSignalFxEvent(event);
        return axios.post(this.SIGNALFX_EVENT_API, [signalfxEvent], this.config);
    }
}