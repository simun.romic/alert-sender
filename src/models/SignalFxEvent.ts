import { Domain } from '.';

export class SignalFxEvent implements Domain {
    public readonly category: string;
    
    public readonly eventType: string;
    
    public readonly dimensions: any;
    
    public readonly properties: any;
    
    public readonly timestamp: number;

    constructor(properties: any) {
        this.category = 'USER_DEFINED';
        this.eventType = 'ec2Status';
        this.dimensions = {
            'environment': 'production',
            'service': 'API'
        };
        this.properties = properties;
        this.timestamp = Date.now();
    }
}