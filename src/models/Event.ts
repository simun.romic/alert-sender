/**
 * Event class
 * Represents model received in POST request
 */

import { Domain } from "./Domain";
import { MinLength, IsString } from 'class-validator';

export class Event implements Domain {
    @IsString()
    @MinLength(1)
    private readonly id: string;
    
    @IsString()
    @MinLength(1)
    private readonly description: string;

    constructor(id: string, description: string) {
        this.id = id;
        this.description = description;
    }

    get getId(): string {
        return this.id;
    }

    get getDescription(): string {
        return this.description;
    }
};