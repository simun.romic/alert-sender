/**
 * Custom exception which is used to serialize error on backend side to client
 */
export class HttpException extends Error {
    public readonly status: number;
    public readonly message: string;
    
    constructor(status: number, message: string) {
      super(message);
      
      this.status = status;
      this.message = message;
    }
};